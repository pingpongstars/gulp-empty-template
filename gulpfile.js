/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp'),
    jshint = require('gulp-jshint'),
    sass   = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    debug = require('gulp-debug'),
    pug = require('gulp-pug'),
    cleanCSS = require('gulp-clean-css'),
    htmlmin = require('gulp-htmlmin'),
    clean = require('gulp-rimraf'),
    runSequence = require('run-sequence');;


var browserSync = require('browser-sync').create();
const reload = browserSync.reload;


var builddir = "build",
    destdir = "public";

// create a default task and just log a message
gulp.task('default', function() {
  return gutil.log('Gulp is running!');
});


gulp.task('clean-public',function(done){

    return gulp.src("public/*")
        .pipe(clean());
});
gulp.task('clean-build',function(done){
    return gulp.src("build/*")
        .pipe(clean());
});

gulp.task('cleanall',['clean-public','clean-build'],function(done){done();});


gulp.task('jshint', function() {
  return gulp.src('source/javascript/**/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
});

gulp.task('watch', function() {
  gulp.watch('source/js/**/*.js', ['jshint','build-js']);
  gulp.watch('source/scss/**/*.scss', ['build-css']);
  gulp.watch('source/pug/**/*.pug',['build-pug']);
  //gulp.watch('source/pug').on('change',build-pug);
});



gulp.task('build-css', function(done) {
  gulp.src('source/scss/**/*.scss')
    .pipe(sourcemaps.init())  // Process the original sources
    .pipe(sass())
    .pipe(gutil.env.type === 'production' ? cleanCSS() : gutil.noop()) 
    .pipe(sourcemaps.write()) // Add the map to modified source.
    .pipe(gulp.dest(builddir + '/css'))
    .on('end',done);
});

gulp.task('build-js', function(done) {
  gulp.src('source/js2compress/**/*.js')
    .pipe(sourcemaps.init())
      .pipe(concat('bundle.js'))
      //only uglify if gulp is ran with '--type production'
      .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop()) 
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(builddir+'/js'))
    .on("end",done);
});

gulp.task('build-pug',function(done) {
  gulp.src('source/pug/*.pug')
  .pipe(debug({title: 'pug:'}))
  .pipe(pug({basedir:'source/pug',pretty:"  " }))
  .pipe(gutil.env.type === 'production' ? htmlmin({collapseWhitespace: true}) : gutil.noop()) 
  .pipe(gulp.dest(  builddir+ "/html" )).on('end',done);
});

gulp.task('copy-css',['build-css'],function(done){
    gulp.src(builddir+ "/css/**/*")
        .pipe(debug({title: 'css:'}))
        .pipe(gulp.dest(destdir+ '/assets/styles'))
        .on('end',done);
    
});
gulp.task('copy-js',['build-js'],function(done){
    gulp.src(builddir+ "/js/**/*")
        .pipe(debug({title: 'js:'}))
        .pipe(gulp.dest(destdir + "/assets/js"))
        .on('end',done);
    
});
gulp.task('copy-html',['build-pug'],function(done){
    gulp.src(builddir+ "/html/**/*")
        .pipe(debug({title: 'html:'}))
        .pipe(gulp.dest(destdir))
        .on('end',done);
    
});

gulp.task('copy-static',function(done){
    gulp.src('source/staticassets/**/*.*')
        .pipe(debug({title: 'static:'}))
        .pipe(gulp.dest(destdir + '/assets/'))
        .on('end',done);

});

gulp.task('copy-all',['copy-js','copy-css','copy-html','copy-static']);

gulp.task('build',function(done){
    runSequence(
        ['clean-public','clean-build']
        ,
        ['copy-js','copy-css','copy-html']
        ,'copy-static'
        ,done);
    
});

gulp.task('serve',['build'], () => {
  browserSync.init({
    browser: ["google-chrome"],
    notify: false,
    port: 9000,
    server: {
      baseDir: ['public'],
      //routes: {
      //  '/bower_components': 'bower_components'
      //}
    }
  });
  
  gulp.watch([
    'public/*.html',
    'public/assets/**/*'
  ]).on('change', reload);
    
  gulp.watch('source/js2compress/**/*.js', ['copy-js']);
  gulp.watch('source/scss/**/*.scss', ['copy-css']);
  gulp.watch('source/staticassets/**/*.*', ['copy-static']);
  gulp.watch('source/pug/**/*.pug',['copy-html']);

});